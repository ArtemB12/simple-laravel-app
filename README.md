## About

This is a simple app that allows you to create, edit and delete notes via webform. App is build with Laravel framework.

## Learning Laravel

- run 'php artisan serve' in a terminal
- run 'npm run dev' in a different terminal
- check your Apache and MySQL running
- see the app at http://127.0.0.1:8000/
