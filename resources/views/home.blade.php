@extends('layouts.app')

@section('title-block')
    Home page
@endsection

@section('content')
    <h1>Home Page</h1>
    <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque minus nobis praesentium quo sint.
        Aspernatur cupiditate incidunt soluta! A illum incidunt nihil recusandae reiciendis, rerum ullam veritatis voluptates. Ea, sunt?
    </p>

@endsection

@section('aside')
    @parent
    <p>More text...</p>
@endsection
