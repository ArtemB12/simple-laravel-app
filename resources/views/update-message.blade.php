@extends('layouts.app')

@section('title-block')
    Message updating
@endsection


@section('content')
    <h1>Message updating</h1>

    <form action="{{ route('contact-update-submit', $data->id) }}" method="post">
        @csrf

        <div class="form-group">
            <label for="name">Enter a name</label>
            <input type="text" name="name" value="{{$data->name}}" placeholder="Enter a name.." id="name" class="form-control">
        </div>

        <div class="form-group mt-2">
            <label for="email">Email</label>
            <input type="text" name="email" value="{{$data->email}}" placeholder="Enter an email.." id="email" class="form-control">
        </div>

        <div class="form-group mt-2">
            <label for="subject">Subject</label>
            <input type="text" name="subject" value="{{$data->subject}}" placeholder="Enter a subject.." id="subject" class="form-control">
        </div>

        <div class="form-group mt-2">
            <label for="message">Message</label>
            <textarea name="message" id="message" placeholder="Enter a message.." class="form-control">{{$data->message}}</textarea>
        </div>

        <button type="submit" class="btn btn-success mt-2">Update</button>
    </form>
@endsection
